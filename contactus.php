<!DOCTYPE html>
<html lang="bg">
    <head>
        <title> ЕКСТРАПЛАСТ | За Контакти</title>
        <?php include_once 'includes/meta.php' ?>
    </head>
    <body>
        <div class="page">
            <header>                
                <?php include_once 'includes/header.php' ?>
            </header>
            <main>
                <div class="section section-about-us bg-pattern-light ">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2>За контакти</h2>
                            </div>
                        </div>
                        <div class="row bg-white">
                            <div class="col-xs-12 col-md-5">
                                <div class="contactus-box">
                                    <h3>ЕКСТРАПЛАСТ ООД</h3>
                                    <address>
                                        <strong>гр. Трявна, ул. Патриарх Евтимий № 34</strong><br>  <br>  
                                        <abbr title="Телефон">Тел:</abbr> 0898 797 855<br>
                                        <abbr title="Телефон">Тел:</abbr>  0898 23 09 23 <br>


                                        <abbr title="Емайл">Емайл:</abbr> 
                                        <script type="text/javascript">
                                            function gen_mail_to_link2(lhs, rhs) {
                                                document.write("<a rel='nofollow' class='email' href=\"mailto");
                                                document.write(":" + lhs + "@");
                                                document.write(rhs + "\">" + lhs + "@" + rhs + "<\/a>");
                                            }
                                        </script>

                                        <script type="text/javascript">
                                            gen_mail_to_link2('extraplast', 'tryavna.biz');
                                        </script>
                                        <noscript> <em>Email address protected by JavaScript</em>  </noscript>
                                    </address>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-7">
                                <div class="contactus-box">                                 
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2924.782369483332!2d25.462684015472142!3d42.856328279156756!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40a91d049c2c7baf%3A0xdd1a995db90ca844!2z0JHQvtC20LrQvtCy0YbQuA!5e0!3m2!1sbg!2sbg!4v1489678115172" frameborder="0" style="border:0; width:100%; height:100%" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section section-outro">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h3> Свържете се с нас</h3>                             
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 notifications ">
                                <div class="alert alert-success" role="alert"></div>
                                <div class="alert alert-danger" role="alert"></div>
                            </div>
                        </div>
                        <div class="row">
                            <form method="post" action="contact.php">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="name">Име (*)</label>
                                        <input class="form-control" type="text" name="name" id="field-name" placeholder="Име и фамилия" required="">  
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="phone">Телефон (*)</label>          
                                        <input class="form-control"  type="tel" name="phone" id="field-phone" placeholder="0888 99 88 77" required="">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="emailadress">Емайл  (*)</label>    
                                        <input class="form-control"  type="email" name="emailadress" id="field-email" placeholder="email@example.com" required="">  
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="enquiry">Запитване (*)</label>  
                                        <textarea class="form-control"  name="enquiry" rows="7" cols="52" id="field-enquiry" required="" placeholder="Вашето запитване"></textarea><br> 
                                    </div>
                                </div>
                            </form>
                            <div class="col-xs-12">
                                <a class="cta" id="submit-enquiry"> Изпрати</a>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <footer>
                <?php include_once 'includes/footer.php'; ?>
            </footer>
        </div>

    </body>
</html>
