<?php

/**
 *  Custom captcha 
 *  Author Mihail Lakov
 */


$randNumber = rand(10000, 99999);

$image = imagecreatetruecolor(180, 80);
$white = imagecolorallocate($image, 255, 255, 255);
$black = imagecolorallocate($image, 0, 0, 0);

imagefilledrectangle($image, 0, 0, 200, 120, $white);
imagettftext($image, 40 , 0, 10, 60, $black, 'font/arial.ttf', $randNumber);
header("Content-type: image/png");
imagepng($image);

?>


