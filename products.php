<!DOCTYPE html>
<html lang="bg">
    <head>
        <title> Екстра Пласт</title>
        <?php include 'includes/meta.php'; ?>
    </head>
    <body>
        <div class="page">
            <header>                
                <?php include_once 'includes/header.php'; ?>
            </header>
            <main>  
                <div class="section section-our-products bg-pattern-light ">
                    <div class="container">
                        <div class="row">
                            <ul class="nav nav-tabs" role="tablist" id="products">
                                <span class="arrow-products"></span>
                                <li role="presentation" class="active"><a href="#piron-dubeli" aria-controls="home" role="tab" data-toggle="tab">Пирон дюбели</a></li>
                                <li role="presentation"><a href="#universalni-dubeli" aria-controls="profile" role="tab" data-toggle="tab">Универсални дюбели</a></li>
                                <li role="presentation"><a href="#dubeli-vint" aria-controls="messages" role="tab" data-toggle="tab">Дюбели с винт</a></li>
                                <li role="presentation"><a href="#dubeli-toploizolacia" aria-controls="settings" role="tab" data-toggle="tab">Дюбели топло изолация</a></li>
                                <li role="presentation"><a href="#kabelni-vryzki" aria-controls="settings" role="tab" data-toggle="tab">Кабелни връзки</a></li>
                                <li role="presentation"><a href="#svredela" aria-controls="settings" role="tab" data-toggle="tab">Свредла за бетон/метал</a></li>
                                <li role="presentation"><a href="#planki" aria-controls="settings" role="tab" data-toggle="tab">Планки</a></li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="piron-dubeli">
                                    <div class="row">                                     
                                        <div class="col-xs-12">
                                            <h2>Пирон дюбели</h2>
                                            <p>
                                                <strong>Пирон дюбелите</strong> 
                                                са подходящи при бърз монтаж на дървени и метални елементи, рамки за сухо строителство с гипсокартон и други подобни материали. 
                                                Продукта е подходящо решение за фиксиране на детайли в бетон, плътни тухли, естествен камък и други.
                                            </p>
                                            <?php include 'product-tables/piron-dubeli.php'; ?>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="universalni-dubeli">    
                                    <div class="row">   
                                        <div class="col-xs-12">
                                            <h2>Универсални дюбели</h2>
                                            <p><strong>Универсалните дюбели</strong> 
                                                се използват предимно за монтаж на дървени елементи, стоманени профили и други към стени от бетон, плътни тухли и тухли с кухини. Винтовете имат цинкова противокорозионна защита.
                                            </p>
                                            <?php include 'product-tables/universalni-dubeli.php'; ?>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="dubeli-vint">
                                    <div class="row">                                      
                                        <div class="col-xs-12">
                                            <h2>Дюбели с винт</h2>
                                            <p><strong>Дюбелите с винт</strong> 
                                                се използват за монтаж на дървени елементи, стоманени профили и други към стени от бетон, плътни тухли и тухли с кухини. Винтовете са с цинкова противокорозионна защита.
                                            </p>
                                            <?php include 'product-tables/dubeli-s-vint.php'; ?>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="dubeli-toploizolacia">
                                    <div class="row">                                       
                                        <div class="col-xs-12">
                                            <h2>Дюбели за топлоизолация</h2>
                                           
                                            <?php include 'product-tables/dubeli-toplo-izolacia.php'; ?>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="kabelni-vryzki">
                                    <div class="row">

                                        <div class="col-xs-12">
                                            <h2>Кабелни връзки</h2>
                                            <p>
                                                Кабелни връзки, наричани още кабелни превръзки, са добре разпространени крепежни за кабели и проводници изготвени от плосък и гъвкав полимер. 
                                                Използват се еднократно, връзката е еднопосочна - стягане със законтряне, отстраняват се чрез срязване.
                                            </p>
                                            <?php include 'product-tables/kabelni-vruzki.php'; ?>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="svredela">
                                    <div class="row">                                        
                                        <div class="col-xs-12">
                                            <h2>Свредела за бетон и метал</h2>
                                            <p>
                                                Свредлата за метал със слабо заострен връх са подходящи за цветни метали. Това включва алуминий, мед, месинг, цинк, желязо и не неръждаеми стомани
                                             </p>
                                             <p>
                                                  Свредла за бетон се използват за пробиване в зидария и бетон в комбинация с ударна бормашина или ротационен перфоратор (за пробиване в бетон е необходим ротационен перфоратор)
                                             </p>
                                            <?php include 'product-tables/svredela.php'; ?>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="planki">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h2>Планки</h2>                                            
                                            <?php include 'product-tables/planki.php'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>       
                </div>
            </main>
            <footer>
                <?php include_once 'includes/footer.php'; ?>
            </footer>
        </div>

    </body>
</html>
