<!DOCTYPE html>
<html lang="bg">
    <head>
        <title>Планки -  ЕКСТРАПЛАСТ</title>
        <?php include 'includes/meta.php'; ?>
    </head>
    <body>
        <div class="page">
            <header>                
                <?php include_once 'includes/header.php'; ?>
            </header>
            <main>  
                <div class="section section-our-products bg-pattern-light ">
                    <div class="container">
                        <div class="row">                                      
                            <div class="col-xs-12">
                                <div class="tab-content">
                                    <h2>Планки</h2>                                            
                                    <?php include 'product-tables/planki.php'; ?>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </main>
            <footer>
                <?php include_once 'includes/footer.php'; ?>
            </footer>
        </div>

    </body>
</html>
