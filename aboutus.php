<!DOCTYPE html>
<html lang="bg">
    <head>
        <title> ЕКСТРАПЛАСТ | За Нас</title>
        <?php include_once 'includes/meta.php' ?>
    </head>
    <body>
        <div class="page">
            <header>                
                <?php include_once 'includes/header.php' ?>
            </header>
            <main>
                <div class="section section-our-products bg-pattern-light about-us">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2> За нас</h2>
                            </div>
                        </div>
                        <div class="row featurette">
                            <div class="col-md-7">
                                <h3 class="featurette-heading">За фирмата </h3>
                                <p class="lead">                                  
                                    <strong>Фирма ЕКСТРАПЛАСТ</strong> е създадена през 1992 год. в <strong>гр. Трявна</strong> с предмет на дейност производство на пластмасови изделия.
                                    Областта на строителството е приоритет за разработките на фирмата и така тя стига до съвършенство в производството на дюбели за универсално приложение. 
                                    В момента фирмата представя на пазара дюбелите <strong>MISCHO CO.</strong> 
                                    Колекцията е широкообхватна по отношение на диаметри и дължина на дюбелите, както и универсална по приложение в различни видове строителни материали. 
                                    Удължената работна част на дюбелите увеличава многократно шансовете за осъществяване на закрепване в тухла. 
                                    <strong>
                                        „Екстрапласт“ ООД</strong>  е един от големите производители на дюбели за топлоизолация , които са сертифицирани с БТО. Фирмата произвежда дюбели за някои от най-големите дистрибутори в страната. 
                                </p>
                            </div>
                            <div class="col-md-5">
                                <img class="featurette-image img-responsive center-block"  alt="екстрапласт" src="img/office-front.jpg" >
                            </div>
                        </div>
                        <div class="row featurette">
                            <div class="col-md-5">
                                <img class="featurette-image img-responsive center-block" alt="екстрапласт" src="img/proizvodstvo.jpg">
                            </div>
                            <div class="col-md-7">
                                <h3 class="featurette-heading">Производство</h3>
                                <p class="lead">Търговия с крепежни елементи, свредла, планки, анкери, шпилки, винтове за дърво и други.   
                                    Областта на строителството е приоритет за разработките на фирмата и така тя стига до съвършенство в производството на дюбели за универсално приложение.</p>
                                <p class="lead"> Освен дюбели фирмата произвежда и аксесоари за алуминиева и PVC дограма.</p>
                            </div>
                        </div>
                        <div class="row featurette">
                            <div class="col-md-7">
                                <h3 class="featurette-heading">Нашият екип</h3>
                                <p class="lead">Екипа на <strong>ЕКСТРАПЛАСТ</strong> се състои от експерти в областта, които са натрупали богат опит с годините. Работим неуморно, помагаме си един на друг, за да успеем..</p>
                            </div>
                            <div class="col-md-5">
                                <img class="featurette-image img-responsive center-block" alt="екстрапласт" src="img/team.jpg">
                            </div>
                        </div>
                        <div class="row featurette cta-container">
                            <div class="col-xs-12 col-sm-6">
                                <div class="cta-box">
                                    <p>Разгледайте многообразието от продукти, които може да откриете при нас!</p>
                                    <a href="products.php" class="cta"> Продуктов Каталог</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">  
                                <div class="cta-box">
                                    <p>Ако желаете да получите повече информация за някои наш продукт може да се свържете с нас!</p>
                                    <a href="contactus.php" class="cta"> Контакти</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <footer>
                <?php include_once 'includes/footer.php'; ?>
            </footer>
        </div>

    </body>
</html>
