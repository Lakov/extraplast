<div class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <script type="text/javascript">
                    function gen_mail_to_link(lhs,rhs) {
                        document.write("<a rel='nofollow' class='email' href=\"mailto" );
                        document.write(":" + lhs + "@");
                        document.write(rhs + "\">" + lhs + "@" + rhs + "<\/a>");
                    }
                </script>
                <span itemprop="email">
                    <i class="glyphicon glyphicon-envelope" ></i>
                    <script type="text/javascript"> 
                        gen_mail_to_link('extraplast','tryavna.biz');
                    </script>
                    <noscript> <em>Email address protected by JavaScript</em>  </noscript>
                </span>
            </div>
            <div class="col-xs-6 pull-right">
                <span class="pull-right"> 
                    <i class="glyphicon glyphicon-earphone"></i> 
                    <a href="tel: +359 898 797 855"><span itemprop="telephone">0898 797 855</span></a>
                </span>
                <span class="pull-right"> 
                    <i class="glyphicon glyphicon-earphone"></i> 
                    <a href="tel: +359 898 23 09 23"><span itemprop="telephone"> 0898 23 09 23</span></a>
                </span>              
            </div>

        </div>
    </div>
</div>
<div class="main-nav">
    <div class="container">
        <div class="row">

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1 class="navbar-brand" id="logo">
                            <img src="img/logo.png" class="img-responsive" alt="logo">
                        </h1>
                    </div>
                    <div class="collapse navbar-collapse" id="main-menu">
                        <ul class="nav navbar-nav">
                            <span class="arrow"></span>
                            <li><a href="index.php">Начало</a></li>
                            <li><a href="aboutus.php">За нас </a></li>
                            <li class="dropdown-toggle">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Дюбели <span class="caret"></span> 
                                </a> 
                                <ul class="dropdown-menu">
                                    <li><a href="piron-dubeli.php">Пирон дюбели</a></li>
                                    <li><a href="universalni-dubeli.php">Универсални дюбели</a></li>
                                    <li><a href="dubeli-s-vint.php">Дюбели с винт</a></li>                                       
                                    <li><a href="dubeli-toploizolacia.php">Дюбели за топлоизолация</a></li>   
                                </ul>          
                            </li>   
                            <li> 
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Търговска част <span class="caret"></span> 
                                </a> 
                                <ul class="dropdown-menu">
                                    <li><a href="kabelni-vruzki.php">Кабелни връзки</a></li>
                                    <li><a href="svredela.php">Свредла</a></li>
                                    <li><a href="planki.php">Планки</a></li>                                       

                                </ul>  
                            </li>       
                            <li><a href="gallery.php">Галерия </a></li>
                            <li><a href="contactus.php">Контакти </a></li>
                        </ul>                                     
                    </div>                           
                </div>
        </div>
        </nav>
    </div>
</div>