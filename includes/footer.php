<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <h4>ЕКСТРАПЛАСТ</h4>
            <ul class="footer-links">
                <li><a href="aboutus.php">За нас</a></li>
                <li><a href="aboutus.php">Производство</a></li>
                <li><a href="contactus.php">Контакти</a></li>
                <li><a href="contactus.php">Галерия</a></li>
            </ul>
        </div> 
        <div class="col-xs-12 col-md-4">
            <h4>ДЮБЕЛИ</h4>
            <ul class="footer-links">
                <li><a href="piron-dubeli.php">Пирон дюбели</a></li>
                <li><a href="universalni-dubeli.php">Универсални дюбели</a></li>
                <li><a href="dubeli-s-vint.php">Дюбели с винт</a></li>
                <li><a href="dubeli-toploizolacia.php">Дюбели за топлоизолация</a></li>
               
            </ul>
        </div> 
         <div class="col-xs-12 col-md-4">
            <h4>ТЪРГОВСКА ЧАСТ</h4>
            <ul class="footer-links">
                 <li><a href="kabelni-vruzki.php">Кабелни връзки</a></li>         
                <li><a href="svredela.php">Свредла</a></li>              
                <li><a href="planki.php">Планки</a></li>
            </ul>
        </div> 
    </div>
</div>
<div class="container copyright">
    <div class="row">
        <div class="col-xs-6">
            <span>ЕКСТРАПЛАСТ <?php echo date("Y"); ?> &COPY; Всички права запазени.</span>
        </div> 
      
    </div>
</div>
<a id="totop" href="#top" ><span class="glyphicon glyphicon-menu-up"></span></a>
<script>
      var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
      };
      var raf = requestAnimationFrame || mozRequestAnimationFrame ||
          webkitRequestAnimationFrame || msRequestAnimationFrame;
      if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
      else window.addEventListener('load', loadDeferredStyles);
    </script>