<!DOCTYPE html>
<html lang="bg">
    <head>
        <title> Универсални Дюбели - ЕКСТРАПЛАСТ</title>
        <?php include 'includes/meta.php'; ?>
    </head>
    <body>
        <div class="page">
            <header>                
                <?php include_once 'includes/header.php'; ?>
            </header>
            <main>  
                <div class="section section-our-products bg-pattern-light ">
                    <div class="container">
                        <div class="row">                                      
                            <div class="col-xs-12">
                                <div class="tab-content">
                                    <h2>Универсални дюбели</h2>
                                    <p>
                                        <strong>Универсалните дюбели </strong>се използват предимно за монтаж на дървени елементи, стоманени профили и други към стени от бетон, плътни тухли и тухли с кухини.
                                    </p>
                                    <?php include 'product-tables/universalni-dubeli.php'; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </main>
            <footer>
                <?php include_once 'includes/footer.php'; ?>
            </footer>
        </div>

    </body>
</html>
