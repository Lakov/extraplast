<!DOCTYPE html>
<html lang="bg">
    <head>
        <title> ЕкстраПласт | Галерия</title>
        <?php include 'includes/meta.php'; ?>  
    </head>
    <body id="top">
        <noscript id="deferred-styles">
            <link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">    
       </noscript>
        <div class="page">
            <header>                
                <?php include_once 'includes/header.php'; ?>
            </header>
            <main>                
                <div class="section bg-pattern-light ">
                    <div class="container">
                        <div class="row">
                            <div class="fotorama" data-maxheight="600" data-nav="thumbs" data-allowfullscreen="true" data-autoplay="true" data-fit="contain">
                                <?php
                                if (is_dir("img/gallery/")) {
                                    if ($dh = opendir("img/gallery/")) {
                                        while (($file = readdir($dh)) !== false) {
                                            echo "<img src=\"img/gallery/$file\" alt=\"extraplast\" >";
                                        }
                                        closedir($dh);
                                    }
                                }
                                ?>                    
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <footer>
                <?php include_once 'includes/footer.php'; ?>
            </footer>
        </div>
    </body>
</html>
