$(document).ready(function () {
    var bodyClass = $('body').attr('class');
    var bannerHeight = $('.paralax').height();
    var hh = $('header').height();
    $(window).scroll(function () {
        if (bodyClass == 'home') {
            if ($(this).scrollTop() > (bannerHeight + 20)) {
                $('header').addClass("sticky");
                $('#totop').css("opacity", "0.9");
                var headerHeight = $('header.sticky').height();
                $('body').css("padding-top", headerHeight);
            } else {
                $('header').removeClass("sticky");
                $('#totop').css("opacity", "0");
                $('body').css("padding-top", 0);
            }
        } else {
            if ($(this).scrollTop() > 10) {
                $('header').addClass("sticky");
                $('#totop').css("opacity", "0.9");
                var headerHeight = $('header.sticky').height();
                $('body').css("padding-top", headerHeight);
            } else {
                $('header').removeClass("sticky");
                $('#totop').css("opacity", "0");
                $('body').css("padding-top", 0);
            }
        }
    });



    if ($('#products').length) {
        var url = window.location.hash.substr(1);
        var tabId = 'a[href="#' + url + '"]';
        $(tabId).tab('show');
    }
    var mainNav = document.querySelector('ul.nav');
    var menuItems = mainNav.querySelectorAll('li');
    var arrow = document.querySelector('.arrow');

    menuItems.forEach(function (item) {
        item.addEventListener('mouseover', function (e) {
            var that = this;
            var itemRect = that.getBoundingClientRect();
            arrow.style.width = itemRect.width + "px";
            arrow.style.left = that.offsetLeft + "px";
        });
    });

    mainNav.addEventListener('mouseleave', function (e) {
        arrow.style.width = "0px";
    });

    var productsNav = document.querySelector('ul.nav-tabs');
    if (productsNav != null) {
        var productsNavTabs = productsNav.querySelectorAll('li');
        var arrowProducts = document.querySelector('.arrow-products');
        productsNavTabs.forEach(function (item) {
            item.addEventListener('mouseover', function (e) {
                var that = this;
                var itemRect = that.getBoundingClientRect();
                arrowProducts.style.width = itemRect.width + "px";
                arrowProducts.style.left = that.offsetLeft + "px";
            });

        });
        productsNav.addEventListener('mouseleave', function (e) {
            arrowProducts.style.width = "0px";
        });
    }

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    $(document).on('click', 'input.alert-danger', function () {
        $(this).removeClass('alert-danger');
    });
    $(document).on('click', 'textarea.alert-danger', function () {
        $(this).removeClass('alert-danger');
    });
    $(document).on('click', '#submit-enquiry', function () {
        var $validate = 1;
        if ($('#field-name').val().length < 3) {
            $('#field-name').addClass('alert-danger');
            $validate = 0;
        }
        if ($('#field-phone').val().length < 5) {
            $('#field-phone').addClass('alert-danger');
            $validate = 0;
        }
        if ($('#field-email').val().length < 4 || !isEmail($('#field-email').val())) {
            $('#field-email').addClass('alert-danger');
            $validate = 0;
        }
        if ($('#field-enquiry').val().length < 4) {
            $('#field-enquiry').addClass('alert-danger');
            $validate = 0;
        }

        if ($validate == 1) {
            var dataString = 'name=' + $("#field-name").val() + '&phone=' + $("#field-phone").val() + '&email=' + $("#field-email").val() + '&enquiry=' + $("#field-enquiry").val();

            $.ajax({
                type: "POST",
                url: "contact-action.php",
                data: dataString,
                cache: false,
                success: function (result) {
                    $('#field-name').val('');
                    $('#field-phone').val('');
                    $('#field-email').val('');
                    $('#field-enquiry').val('');
                    $('.notifications .alert-success').text("Съобщението е успешно изпратено!").show();
                    setTimeout(
                            function ()
                            {
                                $('.notifications').hide();
                            }, 5000);

                },
                error: function (result) {
                    $('#field-name').val('');
                    $('#field-phone').val('');
                    $('#field-email').val('');
                    $('#field-enquiry').val('');
                    $('.notifications .alert-danger').text("Възникна грешка, моля опитайте отново!").show();
                    setTimeout(
                            function ()
                            {
                                $('.notifications').hide();
                            }, 5000);
                }

            });
        }
    });
});


