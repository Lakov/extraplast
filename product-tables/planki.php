<?php ?>
<h3>ЪГЛИ</h3>
<img class="img-responsive" alt="ygli" src="img/products/ygli.jpg">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Цвят</th>
            <th>Наименование</th>
            <th>Брой в пакет</th>            
        </tr>
    </thead>
    <tbody>
        <tr><td>ЦИНК</td><td>ЪГЪЛ 20/20</td><td> 50 </td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ 30/30</td><td> 50 </td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ 40/40</td><td> 50 </td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ 50/50</td><td> 50 </td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ 60/60</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ 80/80</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ 100/100</td><td> 25 </td></tr>
    </tbody>
</table>


<h3>ЪГЛИ КОНЗОЛИ</h3>
<img class="img-responsive" alt="ygli" src="img/products/ygli-konzoli.jpg">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Цвят</th>
            <th>Наименование</th>
            <th>Брой в пакет</th>            
        </tr>
    </thead>
    <tbody>
        <tr><td>ЦИНК</td><td>ЪГЪЛ 40/40 ДВОЙНООРЕБРЕН</td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ КОНЗОЛА ИЗВИТА 30/40</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ КОНЗОЛА ИЗВИТА 48/60</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ КОНЗОЛА ИЗВИТА 67/80</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ КОНЗОЛА ИЗВИТА 86/100</td><td> 25 </td></tr>
    </tbody>
</table>



<h3>ПЛОСКИ ЪГЛИ</h3>
<img class="img-responsive" alt="ploski-ygli" src="img/products/ploski-ygli.jpg">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Цвят</th>
            <th>Наименование</th>
            <th>Брой в пакет</th>            
        </tr>
    </thead>
    <tbody>
        <tr><td>ЦИНК</td><td>ЪГЪЛ 40/40 ДВОЙНООРЕБРЕН</td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ КОНЗОЛА ИЗВИТА 30/40</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ КОНЗОЛА ИЗВИТА 48/60</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ КОНЗОЛА ИЗВИТА 67/80</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ЪГЪЛ КОНЗОЛА ИЗВИТА 86/100</td><td> 25 </td></tr>
    </tbody>
</table>





<h3>ТРИЪГЪЛНИЦИ</h3>
<img class="img-responsive" alt="planki" src="img/products/triygylnici.jpg">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Цвят</th>
            <th>Наименование</th>
            <th>Брой в пакет</th>            
        </tr>
    </thead>
    <tbody>
        <tr><td>ЦИНК</td><td>ТРИЪГЪЛНИК 70 Х 1.5 Х 70</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ТРИЪГЪЛНИК 80 Х 1 Х 80</td><td> 50 </td></tr>
        <tr><td>ЦИНК</td><td>ТРИЪГЪЛНИК 90 Х 2 Х 90</td><td> 25 </td></tr>
    </tbody>
</table>

<h3>ВИНКЕЛИ</h3>
<img class="img-responsive" alt="vinkeli" src="img/products/vinkeli.jpg">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Цвят</th>
            <th>Наименование</th>
            <th>Брой в пакет</th>            
        </tr>
    </thead>
    <tbody>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ 20/20</td><td> 50 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ 25/30</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ 30/30</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ 40/40</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ 50/50</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ 60/60</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ 70/70</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ 80/80</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ 100/100</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ 35/35/130</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ 1.5/30/30/90</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ УСИЛЕН 2.5 Х 55Х 70 Х 70</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ УСИЛЕН 2.5 Х 65 Х 90 Х 90</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ УСИЛЕН 2.5 Х 90 Х 100 Х 100</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ВИНКЕЛ УСИЛЕН 2.5 Х 90 Х 75 Х 130</td><td> 25 </td></tr>
    </tbody>
</table>

<h3>СКОБИ</h3>
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Цвят</th>
            <th>Наименование</th>
            <th>Брой в пакет</th>            
        </tr>
    </thead>
    <tbody>
        <tr><td>ЦИНК</td><td>СКОБА 1/2 </td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>СКОБА 1/2 ТЯСНА</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>СКОБА 1/4</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>СКОБА 1/4 ДВОЙНА </td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>СКОБА 1/4 ЕДНОСТРАННА</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>СКОБА 3/4</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>СКОБА УСИЛЕНА </td><td> 25 </td></tr>
    </tbody>
</table>

<h3>ПЛАНКИ КОНЗОЛНИ</h3>

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Цвят</th>
            <th>Наименование</th>
            <th>Брой в пакет</th>            
        </tr>
    </thead>
    <tbody>
        <tr><td>ЦИНК</td><td>ПЛАНКА КОНЗОЛНА 20/40</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ПЛАНКА КОНЗОЛНА 30/60</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ПЛАНКА КОНЗОЛНА 40/80</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ПЛАНКА КОНЗОЛНА 40/100</td><td> 25 </td></tr>
    </tbody>
</table>


<h3>ПРАВИ ПЛАНКИ</h3>
<img class="img-responsive" alt="plankit" src="img/products/pravi-planki.jpg">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Цвят</th>
            <th>Наименование</th>
            <th>Брой в пакет</th>            
        </tr>
    </thead>
    <tbody>
        <tr><td>ЦИНК</td><td>ПРАВА ПЛАНКА 16/40</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ПРАВА ПЛАНКА 16/60</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ПРАВА ПЛАНКА 16/80</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ПРАВА ПЛАНКА 16/100</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ПРАВА ПЛАНКА 25/80</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ПРАВА ПЛАНКА 25/120</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ПРАВА ПЛАНКА 25/160</td><td> 25 </td></tr>
        <tr><td>ЦИНК</td><td>ПРАВА ПЛАНКА 25/200</td><td> 25 </td></tr>
    </tbody>
</table>




<h3>СТОЙКА ЗА КОЛОНА</h3>
<img class="img-responsive" alt="stoika-kolona" src="img/products/stoika-kolona.jpg">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Цвят</th>
            <th>Наименование</th>
            <th>Брой в пакет</th>            
        </tr>
    </thead>
    <tbody>
        <tr><td>ЧЕРНО</td><td>СТОЙКА ЗА КОЛОНА 65 К-КТ</td><td> 2 </td></tr>
    </tbody>
</table>

