<?php ?>
<img src="img/dubel-toploizolacia.jpg" alt="продукт" class="img-responsive" style="max-width:300px;">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Наименование</th>
            <th>Брой в пакет</th>
            <th>Брой в кашон</th>
        </tr>
    </thead>
    <tbody>
        <tr class="no-hover"><td colspan="3"><img src="img/products/dubel-toploizolacia-2.jpg" alt="продукт" class="img-responsive" style="max-width:300px;"></td></tr>
        <tr><td>Дюбел за топлоизолация с пластмасов пирон Ф 10/100</td><td>100</td><td>1000</td></tr>
        <tr><td>Дюбел за топлоизолация с пластмасов пирон Ф 10/120</td><td>100</td><td>800</td></tr>
        <tr><td>Дюбел за топлоизолация с пластмасов пирон Ф 10/140</td><td>100</td><td>600</td></tr>
        <tr><td>Дюбел за топлоизолация с пластмасов пирон Ф 10/160</td><td>100</td><td>500</td></tr>
        <tr><td>Дюбел за топлоизолация с пластмасов пирон Ф 10/180</td><td>100</td><td>500</td></tr>
        <tr><td>Дюбел за топлоизолация с пластмасов пирон Ф 10/200</td><td>100</td><td>500</td></tr>
        <tr  class="no-hover"><td colspan="3"><img src="img/products/diubel-za-beton.jpg" alt="продукт" class="img-responsive" style="max-width:300px;"></td></tr>
        <tr><td>Дюбел за топлоизолация за бетон ф 8/50</td><td>100</td><td>2000</td></tr>
        <tr><td>Дюбел за топлоизолация за бетон ф 8/70</td><td>100</td><td>2000</td></tr>
        <tr><td>Дюбел за топлоизолация за бетон ф 8/90</td><td>100</td><td>1000</td></tr>
        <tr><td>Дюбел за топлоизолация за бетон ф 8/110</td><td>100</td><td>1000</td></tr>
        <tr><td>Дюбел за топлоизолация за бетон ф 8/130</td><td>100</td><td>1000</td></tr>
        <tr><td>Дюбел за топлоизолация за бетон ф 8/150</td><td>100</td><td>1000</td></tr>
        <tr><td>Дюбел за топлоизолация за бетон ф 8/170</td><td>100</td><td>800</td></tr>
        <tr class="no-hover"><td colspan="3"><img src="img/products/shaiba.jpg" alt="продукт" class="img-responsive" style="max-width:200px;"></td></tr>
        <tr  ><td>Шайба за топлоизолация 60мм</td><td>500</td><td>2000</td></tr>
    </tbody>
</table>