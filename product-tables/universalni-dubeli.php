<?php ?>
<h3>Универсални Дюбели</h3>
<img class="img-responsive" alt="dubel" src="img/products/dubel-universalen-2.png">

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Наименование</th>
            <th>Брой в пакет</th>  
        </tr>
    </thead>
    <tbody>
        <tr><td>Дюбел универсален ф 6/30</td><td>200</td></tr>
        <tr><td>Дюбел мулти ф 6/60</td><td>200</td></tr>
        <tr><td>Дюбел универсален ф 8/40</td><td>200</td></tr>
        <tr><td>Дюбел универсален ф 8/60 </td><td>200</td></tr>
        <tr><td>Дюбел универсален ф 8/80 </td><td>100</td></tr>
        <tr><td>Дюбел универсален ф 8/100</td><td>100</td></tr>
        <tr><td>Дюбел универсален ф 8/120</td><td>100</td></tr>
        <tr><td>Дюбел универсален ф 10/50</td><td>100</td></tr>
        <tr><td>Дюбел универсален ф 10/80</td><td>100</td></tr>
        <tr><td>Дюбел универсален ф 10/100</td><td>100</td></tr>
        <tr><td>Дюбел универсален ф 10/120</td><td>100</td></tr>
        <tr><td>Дюбел универсален ф 10/140</td><td>100</td></tr>
        <tr><td>Дюбел универсален ф 10/160</td><td>50</td></tr>
        <tr><td>Дюбел универсален ф 10/180</td><td>50</td></tr>
        <tr><td>Дюбел универсален ф 10/200</td><td>50</td></tr>
        <tr><td>Дюбел универсален ф 12/60</td><td>100</td></tr>
        <tr><td>Дюбел универсален ф 12/80</td><td>100</td></tr>
        <tr><td>Дюбел универсален ф 12/100</td><td>100</td></tr>
        <tr><td>Дюбел универсален ф 12/120</td><td>100</td></tr>
        <tr><td>Дюбел универсален ф 12/140</td><td>50</td></tr>
        <tr><td>Дюбел универсален ф 12/160</td><td>50</td></tr>
        <tr><td>Дюбел универсален ф 12/200</td><td>50</td></tr>
        <tr><td>Дюбел универсален ф 14/80</td><td>50</td></tr>
        <tr><td>Дюбел универсален ф 14/100</td><td>50</td></tr>
        <tr><td>Дюбел универсален ф 14/120</td><td>50</td></tr>
        <tr><td>Дюбел универсален ф 14/140</td><td>50</td></tr>
        <tr><td>Дюбел универсален ф 14/160</td><td>50</td></tr>
        <tr><td>Дюбел универсален ф 14/200</td><td>50</td></tr>
    </tbody>
</table>



