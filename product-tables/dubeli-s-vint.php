<?php ?>
<h3>ДЮБЕЛ В КОМПЛЕКТ С ВИНТ ФРЕЗЕНК</h3>
<img class="img-responsive" alt="dubel-s-vint" src="img/products/dubel-vint-frezenk.jpg">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Наименование</th>
            <th>Брой в пакет</th>
            <th>Брой в кашон</th>
        </tr>
    </thead>
    <tbody>
          
        <tr><td>Дюбел ф6/60 в комплект с винт 4/60</td><td>200</td><td>1200</td></tr>
        <tr><td>Дюбел ф 8/40 в комплект с винт 5/40</td><td>100</td><td>1500</td></tr>
        <tr><td>Дюбел ф 8/60 в комплект с винт 5/60</td><td>100</td><td>800</td></tr>
        <tr><td>Дюбел ф 8/80 в комплект с винт 5/80</td><td>100</td><td>900</td></tr>
        <tr><td>Дюбел ф 8/100 в комплект с винт 5/100</td><td>100</td><td>600</td></tr>
        <tr><td>Дюбел ф 8/120 в комплект с винт 5/120</td><td>100</td><td>600</td></tr>
    </tbody>
</table>
<h3>ДЮБЕЛ В КОМПЛЕКТ С ВИНТ DIN 571</h3>
<img class="img-responsive" alt="dubel-s-vint" src="img/products/dubel-vint-din.jpg">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Наименование</th>
            <th>Брой в пакет</th>
            <th>Брой в кашон</th>
        </tr>
    </thead>
    <tbody>
    

        <tr><td>Дюбел ф 10/50 в комплект винт DIN 571 6/55</td><td>150</td><td>800</td></tr>
        <tr><td>Дюбел ф 10/80 в комплект винт DIN 571 7/80</td><td>100</td><td>400</td></tr>
        <tr><td>Дюбел ф 10/100 в комплект винт DIN 571 7/100</td><td>75</td><td>400</td></tr>
        <tr><td>Дюбел ф 10/120 в комплект винт DIN 571 7/120</td><td>50</td><td>400</td></tr>
        <tr><td>Дюбел ф 10/140 в комплект винт DIN 571 7/140</td><td>50</td><td>250</td></tr>
        <tr><td>Дюбел ф 10/160 в комплект винт DIN 571 7/160</td><td>50</td><td>200</td></tr>
        <tr><td>Дюбел ф 10/180 в комплект винт DIN 571 7/180</td><td>50</td><td>200</td></tr>
        <tr><td>Дюбел ф 10/200 в комплект винт DIN 571 7/200</td><td>50</td><td>200</td></tr>
        <tr><td>Дюбел ф 12/60 в комплект винт DIN 571 8/60</td><td>100</td><td>400</td></tr>
        <tr><td>Дюбел ф 12/80 в комплект винт DIN 571 8/80</td><td>75</td><td>300</td></tr>
        <tr><td>Дюбел ф 12/100 в комплект винт DIN 571 8/100</td><td>50</td><td>200</td></tr>
        <tr><td>Дюбел ф 12/120 в комплект винт DIN 571 8/120</td><td>50</td><td>200</td></tr>
        <tr><td>Дюбел ф 12/140 в комплект винт DIN 571 8/140</td><td>50</td><td>200</td></tr>
        <tr><td>Дюбел ф 12/160 в комплект винт DIN 571 8/160</td><td>50</td><td>150</td></tr>
        <tr><td>Дюбел ф 12/200 в комплект винт DIN 571 8/200</td><td>50</td><td>150</td></tr>
        <tr><td>Дюбел ф 14/80 в комплект винт DIN 571 10/80</td><td>50</td><td>200</td></tr>
        <tr><td>Дюбел ф 14/100 в комплект винт DIN 571 10/100</td><td>50</td><td>300</td></tr>
        <tr><td>Дюбел ф 14/120 в комплект винт DIN 571 10/120</td><td>25</td><td>100</td></tr>
        <tr><td>Дюбел ф 14/140 в комплект винт DIN 571 10/140</td><td>25</td><td>100</td></tr>
        <tr><td>Дюбел ф 14/160 в комплект винт DIN 571 10/160</td><td>25</td><td>100</td></tr>
        <tr><td>Дюбел ф 14/200 в комплект винт DIN 571 10/200</td><td>25</td><td>100</td></tr>
    </tbody>
</table>
<h3>ДЮБЕЛ ЗА МОНТАЖ НА МИВКА </h3>
<img class="img-responsive" alt="dubel-s-vint" src="img/products/dubel-vint-mischo.jpg">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Наименование</th>
            <th>Брой в пакет</th>
            <th>Брой в кашон</th>
        </tr>
    </thead>
    <tbody>
        
        <tr></tr>
        <tr><td> Дюбел за монтаж на мивка ф12/60 </td><td>2</td><td>100</td></tr>
        <tr><td> Дюбел за монтаж на мивка ф12/80 </td><td>2</td><td>100</td></tr>
        <tr><td>Дюбел  за монтаж на мивка ф 12/100</td><td>2</td><td>100</td></tr>
        <tr><td>Дюбел за монтаж на мивка ф 14/ 80</td><td>2</td><td>100</td></tr>
        <tr><td>Дюбел за монтаж на мивка ф 14/120</td><td>2</td><td>100</td></tr>
    </tbody>
</table>       

<h3> ДЮБЕЛ ЗА МОНТАЖ НА БОЙЛЕР </h3>
<img class="img-responsive" alt="dubel-s-vint" src="img/products/dubel-vint-mischo-2.jpg">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Наименование</th>
            <th>Брой в пакет</th>
            <th>Брой в кашон</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>Дюбел за монтаж на бойлер ф 12/80 с кука </td><td>50</td><td>200</td></tr>
        <tr><td>Дюбел за монтаж на бойлер ф 12/100 с кука</td><td>50</td><td>300</td></tr>
        <tr><td>Дюбел за монтаж на бойлер ф 12/120 с кука</td><td>50</td><td>300</td></tr>
        <tr><td>Дюбел за монтаж на бойлер ф 14/100 с кука</td><td>25</td><td>250</td></tr>
        <tr><td>Дюбел за монтаж на бойлер ф14/120  с кука </td><td>25</td><td>200</td></tr>
        <tr><td>Дюбел за монтаж на бойлер ф14/140  с кука </td><td>25</td><td>125</td></tr>
    </tbody>
</table>     
<h3>   ДЮБЕЛ С КУКА ЗА СКЕЛЕ </h3>
<img class="img-responsive" alt="dubel-s-vint" src="img/products/dubel-vint-kuka.jpg">
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Наименование</th>
            <th>Брой в пакет</th>
            <th>Брой в кашон</th>
        </tr>
    </thead>
    <tbody>  
        <tr></tr>
        <tr><td>Дюбел с кука за скеле ф 14/190</td><td>20</td><td>80</td></tr>
    </tbody>
</table>