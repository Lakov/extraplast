<?php ?>
<h3>Свредла за метал ABRABORO</h3>
<img class="img-responsive" alt="svredela" src="img/products/svredela.jpg">

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Наименование</th>
            <th>Брой в пакет</th>
            <th>Диаметър</th>
            <th> Обща / работна</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>0.3</td><td>19/2,5</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>0.4</td><td>20/5,1</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>0.5</td><td>22/6,1</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>0.6</td><td>24/7,1</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>0.7</td><td>29/9,1</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>0.8</td><td>30/10,1</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>0.9</td><td>32/11</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>1</td><td>34/12</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>1.1</td><td>36/14</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>1.2</td><td>38/16</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>1.3</td><td>38/16</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>1.4</td><td>40/18</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>1.5</td><td>40/18</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>1.6</td><td>43/20</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>1.7</td><td>43/20</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>1.8</td><td>46/22</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>1.9</td><td>46/22</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>2</td><td>49/24</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>2.1</td><td>49/24</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>2.2</td><td>53/27</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>2.3</td><td>53/27</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>2.4</td><td>57/30</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>2.5</td><td>57/30</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>2.6</td><td>57/30</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>2.7</td><td>61/33</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>2.8</td><td>61/33</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>2.9</td><td>61/33</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>3</td><td>61/33</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>3.1</td><td>65/36</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>3.2</td><td>65/36</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>3.3</td><td>65/36</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>3.4</td><td>70/39</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>3.5</td><td>70/39</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>3.6</td><td>70/39</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>3.7</td><td>70/39</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>3.8</td><td>75/43</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>3.9</td><td>75/43</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>4</td><td>75/43</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>4.1</td><td>75/43</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>4.2</td><td>75/43</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>4.3</td><td>80/47</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>4.4</td><td>80/47</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>4.5</td><td>80/47</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>4.6</td><td>80/47</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>4.7</td><td>80/47</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>4.8</td><td>86/52</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>4.9</td><td>86/52</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>5</td><td>86/52</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>5.1</td><td>86/52</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>5.2</td><td>86/52</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>5.3</td><td>86/52</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>5.4</td><td>93/57</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>5.5</td><td>93/57</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>5.6</td><td>93/57</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>5.7</td><td>93/57</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>5.8</td><td>93/57</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>5.9</td><td>93/57</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>6</td><td>93/57</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>6.1</td><td>101/63</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>6.2</td><td>101/63</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>6.3</td><td>101/63</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>6.4</td><td>101/63</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>6.5</td><td>101/63</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>6.6</td><td>101/63</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>6.7</td><td>101/63</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>6.8</td><td>109/69</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>6.9</td><td>109/69</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>7</td><td>109/69</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>7.1</td><td>109/69</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>7.2</td><td>109/69</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>7.3</td><td>109/69</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>7.4</td><td>109/69</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>7.5</td><td>109/69</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>7.6</td><td>117/75</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>7.7</td><td>117/75</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>7.8</td><td>117/75</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>7.9</td><td>117/75</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>8</td><td>117/75</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>8.1</td><td>117/75</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>8.2</td><td>117/75</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>8.3</td><td>117/75</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>8.4</td><td>117/75</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>8.5</td><td>117/75</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>8.6</td><td>125/81</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>8.7</td><td>125/81</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>8.8</td><td>125/81</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>8.9</td><td>125/81</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>9</td><td>125/81</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>9.1</td><td>125/81</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>9.2</td><td>125/81</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>9.3</td><td>125/81</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>9.4</td><td>125/81</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>9.5</td><td>125/81</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>9.6</td><td>133/87</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>9.7</td><td>133/87</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>9.8</td><td>133/87</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>9.9</td><td>133/87</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>10</td><td>133/87</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>10</td><td>10.2</td><td>133/87</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>5</td><td>10.5</td><td>133/87</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>5</td><td>11</td><td>142/94</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>5</td><td>11.5</td><td>142/94</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>5</td><td>12</td><td>151/101</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>5</td><td>12.5</td><td>151/101</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>5</td><td>13</td><td>151/101</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>1</td><td>13.5</td><td>160/108</td></tr>
        <tr><td>HSS-R DIN 338 </td><td>1</td><td>14</td><td>160/108</td></tr>
    </tbody>
</table>


<h3>Свредла за бетон Labor</h3>

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Наименование</th>
            <th>Брой в опаковка</th>
        </tr>
    </thead>
    <tbody>
    <tr><td>Свредло за бетон ф 5x85/50Concrete Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон ф 6x100/60 Concrete Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон ф 6Х200/112 Concrete Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон ф 8x120/80 Concrete Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон ф 8Х200/112 Concrete Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон ф 10x120/80 Concrete Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон ф 10Х200/112 Concrete Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон ф 12x150/90 Concrete Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон ф 12Х200/112 Concrete Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон ф 14x150/90 Concrete Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон ф 16x150/90 Concrete Labor</td><td>1</td></tr>
    <tr><td>ПЛОСЪК СЕКАЧ SDS + 20 X 250 Labor</td><td>1</td></tr>
    <tr><td>ШИЛО SDS + 250 MM Labor</td><td>1</td></tr>
</tbody>
</table>

<h3>Свредла за бетон SDS</h3>

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Наименование</th>
            <th>Брой в опаковка</th>
        </tr>
    </thead>
    <tbody>
    <td>Свредло за бетон SDS + ф 5.0 х 160 /100 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 6.0  х 110/ 50 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 6.0 х 160/100 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 6.0 х 210/150 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 6.0 х 250/190 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 6.5 х 160/100 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 6.5 х 210/150 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 8.0 х 160/100 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 8.0 х 210/150 Labor </td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 8.0 х 250/190 Labor  </td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 10.0 х 160/100 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 10.0 х 210/150 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 10.0 х 250/190 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 10.0 х 310/250 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 12.0 х 210/150 Labor </td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 12.0 х 250/190 Labor </td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 12.0 х 310/250 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 12.0 х 450/390 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 14.0 х 210/150 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 14.0 х 250/190 Labor </td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 14.0 х 310/250 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 16.0 х 210/150 Labor </td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 16.0 х 250/190 Labor  </td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 16.0 х 310/250 Labor </td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 16.0 х 450/390 Labor</td><td>1</td></tr>
    <tr><td>Свредло за бетон SDS + ф 16.0 х 600/540 Labor</td><td>1</td></tr>
    </tbody>
</table>


<h3>Свредла за бетон HSS-R DIN </h3>

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Наименование</th>
            <th>Брой в опаковка</th>

        </tr>
    </thead>
    <td>Свредло за метал HSS-R DIN 338, 15.0мм - о-ка 10мм</td><td>1</td></tr>
<tbody>
    <tr><td>Свредло за метал HSS-R DIN 338, 15.5мм - о-ка 10мм</td><td>1</td></tr>
    <tr><td>Свредло за метал HSS-R DIN 338, 16.0мм - о-ка 10мм</td><td>1</td></tr>
    <tr><td>Свредло за метал HSS-R DIN 388, 17.0мм - о-ка 10мм</td><td>1</td></tr>
    <tr><td>Свредло за метал HSS-R DIN 388, 18.0мм - о-ка 10мм</td><td>1</td></tr>
    <tr><td>Свредло за метал HSS-R DIN 388, 19.0мм - о-ка 10мм</td><td>1</td></tr>
    <tr><td>Свредло за метал HSS-R DIN 388, 20.0мм - о-ка 10мм</td><td>1</td></tr>
    <tr><td>Свредло за метал HSS-R DIN 388, 21.0мм - о-ка 10мм</td><td>1</td></tr>
    <tr><td>Свредло за метал HSS-R DIN 388, 22.0мм - о-ка 10мм</td><td>1</td></tr>
    <tr><td>Свредло за метал HSS-R DIN 388, 23.0мм - о-ка 13мм</td><td>1</td></tr>
    <tr><td>Свредло за метал HSS-R DIN 388, 24.0мм - о-ка 13мм </td><td>1</td></tr>
    <tr><td>Свредло за метал HSS-R DIN 388, 25.0мм - о-ка 13мм </td><td>1</td></tr>
</tbody>
</table>