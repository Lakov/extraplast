<!DOCTYPE html>
<html lang="bg">
    <head>
        <title> Дюбели Топлоизолациа - ЕКСТРАПЛАСТ</title>
        <?php include 'includes/meta.php'; ?>
    </head>
    <body>
        <div class="page">
            <header>                
                <?php include_once 'includes/header.php'; ?>
            </header>
            <main>  
                <div class="section section-our-products bg-pattern-light ">
                    <div class="container">
                        <div class="row">                                      
                            <div class="col-xs-12">
                                <div class="tab-content">
                                 <h2>Дюбели за топлоизолация</h2>
                                 <p>
                                     Това са едни от топ продуктите на фирмата. 
                                     Предлагаме решения за закрепване в тухла и бетон. 
                                     Клиновидния пирон предлага оптимално отваряне на дюбела за тухла , което гарантира добро захващане. 
                                     Дюбелте са сертифицирани с БТО издадено от УАСГ.
                                 </p>
                                            <?php include 'product-tables/dubeli-toplo-izolacia.php'; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </main>
            <footer>
                <?php include_once 'includes/footer.php'; ?>
            </footer>
        </div>

    </body>
</html>
