<!DOCTYPE html>
<html lang="bg">
    <head>
        <title> ЕКСТРАПЛАСТ | Пирон дюбели</title>
        <?php include 'includes/meta.php'; ?>
    </head>
    <body>
        <div class="page">
            <header>                
                <?php include_once 'includes/header.php'; ?>
            </header>
            <main>  
                <div class="section section-our-products bg-pattern-light ">
                    <div class="container">
                        <div class="row">                                     
                            <div class="col-xs-12">
                                <div class="tab-content">
                                <h2>Пирон дюбели</h2>
                                <p>
                                    <strong>Пирон дюбелите</strong> 
                                    са подходящи при бърз монтаж на дървени и метални елементи, рамки за сухо строителство с гипсокартон и други подобни материали. 
                                    Продукта е подходящо решение за фиксиране на детайли в бетон, плътни тухли, естествен камък и други.
                                </p>
                                <?php include 'product-tables/piron-dubeli.php'; ?>
                            </div>
                            </div>
                        </div>
                    </div>  
                </div>       

            </main>
            <footer>
                <?php include_once 'includes/footer.php'; ?>
            </footer>
        </div>

    </body>
</html>
