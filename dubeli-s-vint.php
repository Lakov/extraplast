<!DOCTYPE html>
<html lang="bg">
    <head>
        <title> Дюбели с винт - ЕКСТРАПЛАСТ</title>
        <?php include 'includes/meta.php'; ?>
    </head>
    <body>
        <div class="page">
            <header>                
                <?php include_once 'includes/header.php'; ?>
            </header>
            <main>  
                <div class="section section-our-products bg-pattern-light ">
                    <div class="container">
                        <div class="row">                                      
                            <div class="col-xs-12">
                                <div class="tab-content">
                                    <h2>Дюбели с винт</h2>
                                    <p>
                                        <strong>Дюбелите с винт</strong> се използват за монтаж на дървени елементи, стоманени профили и други към стени от бетон, плътни тухли и тухли с кухини. 
                                        Дюбелите са окомплектовани с подходящите за тях винтове .
                                    </p>
                                    <?php include 'product-tables/dubeli-s-vint.php'; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </main>
            <footer>
                <?php include_once 'includes/footer.php'; ?>
            </footer>
        </div>

    </body>
</html>
