<?php
 if($_POST && isset($_POST['name'], $_POST['email'], $_POST['phone'], $_POST['enquiry'])) {

    $name = filter_var($_POST['name'],FILTER_SANITIZE_STRING);
    $email = filter_var($_POST['email'],FILTER_VALIDATE_EMAIL);
    $phone = filter_var($_POST['phone'],FILTER_SANITIZE_STRING);
    $enquiry = filter_var($_POST['enquiry'],FILTER_SANITIZE_STRING);
    $errorMsg= "";
    if(!$name) {
      $errorMsg = "Please enter your Name";
    } elseif(!preg_match("/^\S+@\S+$/", $email)) {
      $errorMsg = "Please enter a valid Email address";
    } elseif(!$enquiry) {
      $errorMsg = "Please enter your comment in the Message box";
    } else {
    
      $message= "Емайл: ".$email." Име: " .$name." Телефон: " .$phone. " Зашитване: ".$enquiry;
      $to = "office@extraplast.net";
      $subject = "Extraplast - запитване от ".$name;
      $headers = 'Content-Type: text/html; charset=UTF-8' . "\r\n";
      $headers.= 'From:'.$name. '<'.$email.'>' . "\r\n";
      $headers .= 'Reply-To:'.$name.' <'.$email.'>' . "\r\n" ;

      mail($to, $subject, $message, $headers); 
      exit;
    }
    return $errorMsg;
  }
  else{echo "ERROR not set";}
?>