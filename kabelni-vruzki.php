<!DOCTYPE html>
<html lang="bg">
    <head>
        <title> Кабелни Връзки - ЕКСТРАПЛАСТ</title>
        <?php include 'includes/meta.php'; ?>
    </head>
    <body>
        <div class="page">
            <header>                
                <?php include_once 'includes/header.php'; ?>
            </header>
            <main>  
                <div class="section section-our-products bg-pattern-light ">
                    <div class="container">
                        <div class="row">                                      
                            <div class="col-xs-12">
                                <div class="tab-content">
                                    <h2>Кабелни връзки</h2>
                                    <p>
                                        <strong>Кабелни връзки</strong>, наричани още свински опашки, са добре разпространени крепежи за кабели и проводници, изработени от плосък и гъвкав полимер. Използват се еднократно, връзката е еднопосочна - стягане със законтряне, отстраняват се чрез срязване. Предлаганите от нас кабелни връзки са на италианската фирма „ELEMATIC“. 

                                    </p>
                                    <?php include 'product-tables/kabelni-vruzki.php'; ?>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </main>
            <footer>
                <?php include_once 'includes/footer.php'; ?>
            </footer>
        </div>

    </body>
</html>
