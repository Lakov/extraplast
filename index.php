<!DOCTYPE html>
<html lang="bg">
    <head>
        <title> ЕКСТРАПЛАСТ | Начална страница</title>

        <?php include 'includes/meta.php'; ?>
    </head>
    <body id="top" class="home">
        <div class="page">
            <div class="paralax">
                <div class="banner-heading">
                    <img src="img/logo-shadow.png" alt="logo" class="img-responsive logo">
                    <h2>
                        Производител на пластмасови дюбели
                    </h2>
                    <img class="img-responsive scroll" src="img/scrolldown_ani.gif">
                </div>
            </div>
            <header>
                <?php include_once 'includes/header.php'; ?>
            </header>
            <main>

                <div class="section section-our-products bg-pattern-light ">

                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2>Нашите продукти </h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="product-box">
                                    <a href="piron-dubeli.php">
                                        <img src="img/piron-dubeli.png" alt="пирон дюбели" class="img-responsive">
                                        <div class="product-info">
                                            <h3> Пирон дюбели</h3>
                                            <p>  Подходящо решение за фиксиране на детайли в бетон, плътни тухли, естествен камък и други</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="product-box">
                                    <a href="universalni-dubeli.php"><img src="img/dubel-universalen.jpg" alt="универсални" class="img-responsive">
                                        <div class="product-info">
                                            <h3> Универсални дюбели</h3>
                                            <p> Универсалните дюбели се използват предимно за монтаж на дървени елементи, стоманени профили...  </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="product-box">
                                    <a href="dubeli-s-vint.php"><img src="img/dubel-vint.png" alt="дюбели с винт" class="img-responsive">
                                        <div class="product-info">
                                            <h3> Дюбели с винт</h3>
                                            <p>Дюбелите с винт се използват за монтаж на дървени елементи, стоманени профили и други... </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="product-box">
                                    <a href="dubeli-toploizolacia.php"><img src="img/dubel-toploizolacia.jpg" alt="топлоизолация" class="img-responsive">
                                        <div class="product-info">
                                            <h3> Дюбели топлоизолация</h3>
                                            <p> Това са едни от топ продуктите на фирмата . Предлагаме решения за закрепване в тухла и бетон. </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="product-box">
                                    <a href="kabelni-vruzki.php"><img src="img/svredela.jpg" alt="кабелни връзки" class="img-responsive">
                                        <div class="product-info">
                                            <h3> Свредла</h3>
                                            <p>Свредлата за метал предлагани от нас са на унгарската фирма ABRABORO.
                                                Свредлата за бетон са на холандската фирма Labor</p>

                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="product-box">
                                    <a href="planki.php"><img src="img/planki.jpg" alt="планки" class="img-responsive">
                                        <div class="product-info">
                                            <h3> Планки</h3>
                                            <p> Планки за допълнително укрепване на детайли</p>
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="section section-about-us">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2> ЕКСТРАПЛАСТ ООД </h2>
                            </div>
                        </div>
                        <div class="row featurette">
                            <div class="col-md-6">
                                <p class="lead">
                                    <strong>Фирма ЕКСТРАПЛАСТ</strong> е създадена през <strong>1992 година</strong> в <strong>гр. Трявна</strong> с предмет на дейност производство на пластмасови изделия.
                                    Областта на строителството е приоритет за разработките на фирмата и така тя стига до съвършенство в производството на дюбели за универсално приложение.
                                    В момента фирмата представя на пазара дюбелите <strong>MISCHO CO.</strong>
                                    Колекцията е широкообхватна по отношение на диаметри и дължина на дюбелите, както и универсална по приложение в различни видове строителни материали.
                                    Удължената работна част на дюбелите увеличава многократно шансовете за осъществяване на закрепване в тухла. 
                                    <strong>„Екстрапласт“ ООД</strong>  е един от големите производители на дюбели за топлоизолация , които са сертифицирани с БТО. Фирмата произвежда дюбели за някои от най-големите дистрибутори в страната. 
                                </p>
                                <div class="cta-box">

                                    <a href="aboutus.php" class="cta"> Научете повече за ЕКСТРАПЛАСТ</a>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <img class="featurette-image img-responsive center-block" alt="екстрапласт" src="img/office-front.jpg">
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="section section-outro">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3> Свържете се с нас</h3>
                    </div>
                </div>
                <div class="row">
                    <form method="post" action="contact.php">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="name">Име (*)</label>
                                <input class="form-control" type="text" name="name" id="field-name" placeholder="Име и фамилия" required="">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="phone">Телефон (*)</label>
                                <input class="form-control"  type="tel" name="phone" id="field-phone" placeholder="0888 99 88 77" required="">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="emailadress">Емайл  (*)</label>
                                <input class="form-control"  type="email" name="emailadress" id="field-email" placeholder="email@example.com" required="">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="enquiry">Запитване (*)</label>
                                <textarea class="form-control"  name="enquiry" rows="7" cols="52" id="field-enquiry" required="" placeholder="Вашето запитване"></textarea><br>
                            </div>
                        </div>

                    </form>
                    <div class="col-xs-12">
                        <a class="cta" id="submit-enquiry"> Изпрати</a>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer>
        <?php include_once 'includes/footer.php'; ?>
    </footer>
</div>
</body>
</html>
