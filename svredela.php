<!DOCTYPE html>
<html lang="bg">
    <head>
        <title>Свредла - ЕКСТРАПЛАСТ</title>
        <?php include 'includes/meta.php'; ?>
    </head>
    <body>
        <div class="page">
            <header>                
                <?php include_once 'includes/header.php'; ?>
            </header>
            <main>  
                <div class="section section-our-products bg-pattern-light ">
                    <div class="container">
                        <div class="row">                                      
                            <div class="col-xs-12">
                                <div class="tab-content">
                                    <h2>Свредла за бетон и метал</h2>
                                    <p>
                                        Свредлата за метал предлагани от нас са на унгарската фирма ABRABORO</p>
                                    <p>Свредлата за бетон са на холандската фирма Labor

                                    </p>
                                    <?php include 'product-tables/svredela.php'; ?>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </main>
            <footer>
                <?php include_once 'includes/footer.php'; ?>
            </footer>
        </div>

    </body>
</html>
